<?php
/**
 * Template Name: Proc my
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>

<div class="rp_proc_page_holder">
    <div class="home_slider_holder">
        <div id="home_slider" class="carousel slide" data-ride="carousel">
            <ul class="carousel-indicators">
                <?php
                if( have_rows('slider') ):
                    $index = 0;
                    while ( have_rows('slider') ) : the_row(); ?>
                        <li data-target="#home_slider" data-slide-to="<?= $index ?>" class="<?= $index == 0 ? 'active':'' ?>"><?= get_sub_field('nazev'); ?></li>
                        <?php
                        $index++;
                    endwhile;
                endif;
                ?>
            </ul>
            <div class="carousel-inner" role="listbox">
                <?php
                if( have_rows('slider') ):
                    $index = 0;
                    while ( have_rows('slider') ) : the_row(); ?>
                        <div class="carousel-item <?= $index == 0 ? 'active':'' ?>" style="background: url(<?= get_sub_field('obrazek'); ?>) center no-repeat / cover;">
                        </div>
                        <?php
                        $index++;
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
    <!--<div class="rp_tabs_mobile">

		<div class="rp_proc_mobile_holder">
			<div class="rp_proc_mobile_item">
				<h2><?/*= get_field('nadpis_zalozky_1'); */?></h2>
				<div class="rp_proc_mobile_item_text">
					<div><?/*= get_field('blok_1_1'); */?></div>
					<div><?/*= get_field('blok_2_1'); */?></div>
					<div><?/*= get_field('blok_3_1'); */?></div>
					<div><?/*= get_field('blok_4_1'); */?></div>
				</div>
			</div>
			<div class="rp_proc_mobile_item">
				<h2><?/*= get_field('nadpis_zalozky_2'); */?></h2>
				<div class="rp_proc_mobile_item_text">
					<div><?/*= get_field('blok_1_2'); */?></div>
					<div><?/*= get_field('blok_2_2'); */?></div>
				</div>
			</div>
			<div class="rp_proc_mobile_item">
				<h2><?/*= get_field('nadpis_zalozky_3'); */?></h2>
				<div class="rp_proc_mobile_item_text">
					<div><?/*= get_field('blok_3_1'); */?></div>
					<div><?/*= get_field('blok_3_2'); */?></div>
					<div><?/*= get_field('blok_3_3'); */?></div>
				</div>
			</div>
		</div>

	</div>-->
</div>
<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
