<div class="rp_reference_editor_text">
    <?= get_field('text_podstranky_reference',get_option( 'page_on_front' )) ?>
</div>
<div class="rp_reference_page_holder">
    <div class="container">
        <div class="row">
            <?php while (have_posts()) : the_post(); ?>
                <div class="col-md-6">
                    <div class="rp_reference_item_holder">
                        <div class="row">
                            <div class="col-lg-7 rp_reference_item_heading_holder">
                                <h3><?= get_the_title() ?></h3>
                                <p><?= get_field('podnadpis') ?></p>
                            </div>
                            <div class="col-lg-5 rp_reference_item_logo_holder">
                                <img src="<?= get_field('logo') ?>" alt="<?= get_the_title() ?>">
                            </div>
                            <div class="col-lg-12">
                                <div class="rp_reference_item_img"
                                     style="background: url(<?= get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>) 0 0 / cover no-repeat;"></div>
                            </div>
                            <div class="col-lg-12">
                                <p><?= get_field('kratky_popis') ?>...</p>
                                <a class="rp_produkt_more_btn"
                                   href="<?= get_permalink() ?>"><?= __('read more', 'cipres') ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>


