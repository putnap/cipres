<?php
/**
 * Template Name: Home
 */

$args = array(
    'post_status' => 'publish',
    'post_type' => 'slider',
    'posts_per_page' => -1,
    'orderby' => 'date',
    'order' => 'DESC'
);
$slider_posts = new \WP_Query($args);
$index = 0;
$index_slidu = 0;
?>
<div class="owl-carousel_slid owl-carousel owl-theme">
    <?php while ($slider_posts->have_posts()) : $slider_posts->the_post() ?>
        <img class="d-block img-fluid"
             src="<?= get_the_post_thumbnail_url(get_the_ID(), 'full') ?>"
             alt="<?= get_the_title(); ?>">
        <?php
        $index_slidu++;
    endwhile;
    wp_reset_query(); ?>
</div>
<div class="rp_czech_flag_holder clearfix">
    <div class="rp_czech_flag_text"><?= __('Made in<br>czech republic', 'cipres'); ?></div>
</div>
<div class="rp_aplikace_holder">
    <div class="rp_aplikace_heading">
        <h3><?= get_field('nadpis_aplikace_na_uvodni_strance') ?></h3>
    </div>
    <div class="aplikace_slider_holder">
        <div class="owl-carousel owl-theme aplikace_slider">
            <?php
            $args = array(
                'post_status' => 'publish',
                'post_type' => 'aplikace',
                'posts_per_page' => -1,
                'orderby' => 'date',
                'order' => 'DESC'
            );
            $aplikace_posts = new \WP_Query($args);

            while($aplikace_posts->have_posts()) : $aplikace_posts->the_post() ?>
            <div class="item">
                <a href="<?= get_permalink() ?>">
                    <div class="aplikace_img" style="background: url(<?= get_the_post_thumbnail_url(get_the_ID(),'medium') ?>)"></div>
                    <div class="aplikace_text">
                        <div><?= get_the_title() ?></div>
                    </div>
                </a>
            </div>
            <?php  endwhile;
            wp_reset_query(); ?>
        </div>
        <div class="owl_custom_navigation">
            <div class="aplikace_prev"></div>
            <div class="aplikace_next"></div>
        </div>
    </div>
</div>
<?php while (have_posts()) : the_post(); ?>
<?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
