<div class="rp_page_stazeni_holder">
    <div id="accordion" class="rp_stazeni_accordion" role="tablist" aria-multiselectable="true">

        <?php
        $terms = get_terms('typ', array(
            'hide_empty' => true,
            'parent' => 0
        ));
        $index = 0;
        foreach ($terms as $term) { ?>
            <div class="rp_stazeni_item_holder">
                <div class="rp_stazeni_item_header" role="tab" id="heading<?= $index ?>">
                    <h2>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $index ?>"
                           aria-expanded="false"
                           aria-controls="collapse<?= $index ?>">
                            <?= $term->name ?>
                        </a>
                    </h2>
                    <p><?= $term->description ?></p>
                    <a class="rp_panel_btn" data-toggle="collapse" data-parent="#accordion"
                       href="#collapse<?= $index ?>"
                       aria-expanded="false" aria-controls="collapse<?= $index ?>">
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                </div>
                <div id="collapse<?= $index ?>" class="collapse" role="tabpanel" aria-labelledby="heading<?= $index ?>">

                    <?php

                    if (get_term_children($term->term_id, 'typ')) {
                        $index_child = 0;
                        foreach (get_term_children($term->term_id, 'typ') as $item) {
                            $term_object = get_term($item);
                            if ($term_object->count != 0) { ?>
                                <div id="accordion" class="rp_stazeni_accordion sub_accord" role="tablist"
                                     aria-multiselectable="true">

                                    <div class="rp_stazeni_item_holder">
                                        <div class="rp_stazeni_item_header" role="tab" id="heading0">
                                            <h2>
                                                <a data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapse_sub<?= $index_child ?>"
                                                   aria-expanded="false" aria-controls="collapse" class="collapsed">
                                                    <?= $term_object->name ?> </a>
                                            </h2>
                                            <p><?= $term_object->description ?></p>
                                            <a class="rp_panel_btn collapsed" data-toggle="collapse"
                                               data-parent="#accordion" href="#collapse_sub<?= $index_child ?>"
                                               aria-expanded="false"
                                               aria-controls="collapse">
                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                        <div id="collapse_sub<?= $index_child ?>" class="collapse" role="tabpanel"
                                             aria-labelledby="heading"
                                             aria-expanded="false" style="">

                                            <?php
                                            $args = array(
                                                'post_type' => 'soubory',
                                                'tax_query' => array(
                                                    array(
                                                        'taxonomy' => 'typ',
                                                        'field' => 'id',
                                                        'terms' => $item
                                                    )
                                                )
                                            );
                                            $query2 = new WP_Query($args);

                                            while ($query2->have_posts()) : $query2->the_post();
                                                $soubor = get_field('soubor', get_the_ID()); ?>
                                                <div class="rp_soubor_item">
                                                    <a class="soubor" href="<?= $soubor['url'] ?>" target="_blank">
                                                        <div class="soubor_ico">
                                                            <i class="fa fa-file" aria-hidden="true"></i>
                                                        </div>
                                                        <div><?= get_the_title(); ?></div>
                                                    </a>
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                    </div>


                                </div>
                                <?php
                            }
                            $index_child++;
                        }

                    } else {

                        $args = array(
                            'post_type' => 'soubory',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'typ',
                                    'field' => 'id',
                                    'terms' => $term->term_id
                                )
                            )
                        );
                        $query = new WP_Query($args);
                        while ($query->have_posts()) : $query->the_post();
                            $soubor = get_field('soubor', get_the_ID());
                            ?>
                            <div class="rp_soubor_item">
                                <a class="soubor" href="<?= $soubor['url'] ?>" target="_blank">
                                    <div class="soubor_ico">
                                        <i class="fa fa-file" aria-hidden="true"></i>
                                    </div>
                                    <div><?= get_the_title(); ?></div>
                                </a>
                            </div>
                            <?php
                        endwhile;
                        wp_reset_query();

                    }
                    ?>

                </div>
            </div>
            <?php
            $index++;
        }
        ?>
    </div>
</div>

