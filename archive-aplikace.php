<div class="rp_produkty_editor_text">
    <?= get_field('text_podstranky_aplikace',get_option( 'page_on_front' )) ?>
</div>

<div class="rp_aplikace_page_holder">
	<div class="container">
		<div class="row no-gutters">
            <?php
            while (have_posts()) : the_post(); ?>
                <div class="col-md-3 col-sm-4">
                    <div class="rp_aplikace_page_item">
                        <a href="<?= get_permalink() ?>">
                            <div class="rp_aplikace_page_img" style="background: url(<?= get_the_post_thumbnail_url(get_the_ID(),'medium') ?>)"></div>
                            <div class="rp_aplikace_page_name"><?= get_the_title() ?></div>
                        </a>
                    </div>
                </div>
            <?php endwhile; ?>
		</div>
	</div>
</div>


