/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {

          $('.owl-carousel_slid').owlCarousel({
              loop:true,
              items:1,
              margin:10,
              autoplay:true,
              autoplayTimeout:5000,
              autoplayHoverPause:true
          });
        // JavaScript to be fired on all pages

          var windowWidth = $(window).width();

          //Aplikace slider settings
          $('.aplikace_slider').owlCarousel({
              loop:true,
              margin:10,
              nav:false,
              dots:false,
              responsive:{
                  0:{
                      items:2
                  },
                  768:{
                      items:3
                  },
                  1000:{
                      items:5
                  },
                  1200:{
                      items:7
                  }

              }
          });

          var owl = $('.aplikace_slider').owlCarousel();

          $(".aplikace_prev").click(function () {
              owl.trigger('prev.owl.carousel');
          });

          $(".aplikace_next").click(function () {
              owl.trigger('next.owl.carousel');
          });


          //Side menu function
          $('.mobile_menu_btn').click(function(e){
              if(!$(this).hasClass("is-active")){
                  $('.header_main_mobile_menu').animate({ right: '0' }, 700);
              }

              $(this).addClass("is-active");
              e.preventDefault();
          });

          $('.header_main_close_btn a').click(function(e){

              $('.header_main_mobile_menu').animate({ right: '-100%' }, 700);
              $('.mobile_menu_btn').removeClass("is-active");
              e.preventDefault();
          });






          //Custom selectbox
          //TOGGLING NESTED ul
          $(".drop-down-select .selected a").click(function() {
              $(".drop-down-select .options ul").toggle();
          });

          //SELECT OPTIONS AND HIDE OPTION AFTER SELECTION
          $(".drop-down-select .options ul li a").click(function() {
              var text = $(this).html();
              console.log($(this).html());
              $(".drop-down-select .selected a span").html(text);
              $(".drop-down-select .options ul").hide();
          });

          //HIDE OPTIONS IF CLICKED ANYWHERE ELSE ON PAGE
          $(document).bind('click', function(e) {
              var $clicked = $(e.target);
              if (! $clicked.parents().hasClass("drop-down-select")){
                  $(".drop-down-select .options ul").hide();
              }
          });


          //Proc my tabs
          if($('.rp_tabs_desktop').length && windowWidth > 768){
              $('.rp_proc_menu_holder div').on('click', function(){
                  var tab_id = $(this).attr('data-tab');
                  var bgPath = $(this).attr('data-img');

                  //Zmena obrazku v pozadi
                  $('.rp_proc_content_holder').css('background', 'url(' + bgPath + ')' );

                  $('.rp_proc_menu_holder div.active').removeClass('active');
                  if('.rp_proc_content_holder .rp_proc_content:visible'){
                      $('.rp_proc_content_holder .rp_proc_content').fadeOut();
                  }

                  $(this).addClass('active');
                  $('#' + tab_id).show().fadeIn();
              });
          }


          //Napiste nam popup
          $('.open-popup-link').magnificPopup({
              type:'inline',
              midClick: true,
              closeBtnInside: true
          });





      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
