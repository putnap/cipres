<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup()
{
    // Enable features from Soil when plugin is activated
    // https://roots.io/plugins/soil/
    add_theme_support('soil-clean-up');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-relative-urls');
    add_theme_support('soil-disable-trackbacks');
    add_theme_support('soil-disable-asset-versioning');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-js-to-footer');

    add_theme_support( 'custom-logo' );

    // Make theme available for translation
    // Community translations can be found at https://github.com/roots/sage-translations
    load_theme_textdomain('sage', get_template_directory() . '/lang');

    // Enable plugins to manage the document title
    // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
    add_theme_support('title-tag');

    // Register wp_nav_menu() menus
    // http://codex.wordpress.org/Function_Reference/register_nav_menus
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'mobile_navigation' => __('Mobile', 'sage')
    ]);

    // Enable post thumbnails
    // http://codex.wordpress.org/Post_Thumbnails
    // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
    // http://codex.wordpress.org/Function_Reference/add_image_size
    add_theme_support('post-thumbnails');
    add_image_size('produkt', 160, 290);

    // Enable post formats
    // http://codex.wordpress.org/Post_Formats
    add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

    // Enable HTML5 markup support
    // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    // Use main stylesheet for visual editor
    // To add custom styles edit /assets/styles/layouts/_tinymce.scss
    add_editor_style(Assets\asset_path('styles/main.css'));

    show_admin_bar(false);

}

add_action('after_setup_theme', __NAMESPACE__ . '\\setup');
/**
 * Register sidebars
 */
function widgets_init()
{
    register_sidebar([
        'name' => __('Footer 1', 'sage'),
        'id' => 'footer-1',
        'before_widget' => ' ',
        'after_widget' => ' ',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ]);
    register_sidebar([
        'name' => __('Footer 2', 'sage'),
        'id' => 'footer-2',
        'before_widget' => ' ',
        'after_widget' => ' ',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ]);

    register_sidebar([
        'name' => __('Footer 3', 'sage'),
        'id' => 'footer-3',
        'before_widget' => ' ',
        'after_widget' => ' ',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ]);
}

add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');


/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar()
{
    static $display;

    isset($display) || $display = !in_array(true, [
        // The sidebar will NOT be displayed if ANY of the following return true.
        // @link https://codex.wordpress.org/Conditional_Tags
        is_404(),
        is_front_page(),
        is_page_template('template-custom.php'),
    ]);

    return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets()
{
	wp_enqueue_style('awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false, null);
    wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);


    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
}

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);


add_action('init', __NAMESPACE__ . '\\posty');
function posty()
{
    register_post_type('aplikace',
        array(
            'labels' => array(
                'name' => __('Aplikace'),
                'singular_name' => __('Aplikace')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'aplikace'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            ),
        )
    );
    register_post_type('reference',
        array(
            'labels' => array(
                'name' => __('Reference'),
                'singular_name' => __('Reference')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'reference'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            ),
        )
    );
    register_post_type('produkt',
        array(
            'labels' => array(
                'name' => __('Produkty'),
                'singular_name' => __('Produkt')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'produkt'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            ),
        )
    );
    register_post_type('slider',
        array(
            'labels' => array(
                'name' => __('Slider'),
                'singular_name' => __('Slider')
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'slider'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            ),
        )
    );
    register_post_type('soubory',
        array(
            'labels' => array(
                'name' => __('Soubory'),
                'singular_name' => __('Soubor')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'soubory'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            ),
        )
    );
}

add_action( 'init',  __NAMESPACE__ . '\\create_soubor_tax' );

function create_soubor_tax() {
    register_taxonomy(
        'typ',
        'soubory',
        array(
            'label' => 'Kategorie',
            'hierarchical' => true,
        )
    );
}

function my_et_builder_post_types( $post_types ) {
	$post_types[] = 'produkt';
	$post_types[] = 'aplikace';
	$post_types[] = 'reference';
	/*$post_types[] = 'ANOTHER_CPT_HERE';*/

	return $post_types;
}
add_filter( 'et_builder_post_types', __NAMESPACE__ . '\\my_et_builder_post_types' );
