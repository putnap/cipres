<footer class="container">
    <?php /*dynamic_sidebar('sidebar-footer'); */?>
    <div class="main_footer">
        <div class="row">
            <div class="col-12">
                <h3><?= __('contacts', 'RP') ?></h3>
            </div>
            <div class="col-md-5 col-12">
                <div class="main_footer-gradient">
                    <div><?= __('headquarters', 'RP') ?></div>
                    <div>
                        <?php dynamic_sidebar( 'footer-1' ); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-12">
                <div class="main_footer-gradient">
                    <div><?= __('production', 'RP') ?></div>
                    <div>
                        <?php dynamic_sidebar( 'footer-2' ); ?>
                    </div>
                </div>
            </div>
            <div class="main_footer-prehled">
                <div>
                    <?php dynamic_sidebar( 'footer-3' ); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="sub_footer">
        <div>© Cipres Filtr Brno, s.r.o., 2017 | <?= __('all rights reservered', 'RP') ?></div>
        <div><a href="/autoservis"><?= __('visit our company car service', 'RP') ?></a></div>
    </div>
</footer>
