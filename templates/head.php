<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1029700330;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1029700330/?guid=ON&amp;script=0"/>
        </div>
    </noscript>

    <script type="text/javascript">
        /* <![CDATA[ */
        var seznam_retargeting_id = 13631;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//c.imedia.cz/js/retargeting.js"></script>

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA 3540346-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->

  <?php wp_head(); ?>
</head>
