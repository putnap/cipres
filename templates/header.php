<?php

use Roots\Sage\Extras;

$jazyky = icl_get_languages('skip_missing=0&orderby=CUSTOM&link_empty_to=str');

?>


<header class="banner">
    <div class="container header_desktop">
        <div class="header_top">
            <?php
            if (function_exists('the_custom_logo')) {
                the_custom_logo();
            }
            ?>
            <div class="rp_header_slogan"><?= get_bloginfo('description') ?></div>
        </div>
        <div class="header_menu">
            <nav class="navbar navbar-toggleable-md navbar-inverse">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                        data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <?php
                        $menuLocations = get_nav_menu_locations();
                        $menuID = $menuLocations['primary_navigation'];
                        $primaryNav = wp_get_nav_menu_items($menuID);

                        global $wp;
                        $current_url = home_url($wp->request);
                        foreach ($primaryNav as $item) { ?>
                            <li class="nav-item <?= $current_url . '/' == $item->url ? 'active' : '' ?>">
                                <a class="nav-link" href="<?= $item->url ?>"><?= $item->title ?></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <div class="language_holder">
                        <div class="drop-down-select">
                            <div class="selected">
                                <?php
                                foreach ($jazyky as $jazyk) {
                                    if ($jazyk['active'] == 1): ?>
                                        <a href="#"><span><img
                                                        src="<?= $jazyk['country_flag_url'] ?>" alt=""></span></a>
                                    <?php endif;
                                } ?>
                            </div>
                            <div class="options">
                                <ul>
                                    <?php
                                    foreach ($jazyky as $jazyk) {
                                        if (isset($jazyk['missing']) && $jazyk['missing'] == 1) {
                                            switch ($jazyk['code']) {
                                                case 'bg': ?>
                                                    <li><a href="//seznam.cz"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'nl': ?>
                                                    <li><a href="http://www.cipres.de.com/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'de': ?>
                                                    <li><a href="http://www.cipres.de.com/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'ru': ?>
                                                    <li><a href="http://www.cipres.ru/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'ch': ?>
                                                    <li><a href="http://www.cipres.de.com/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'en': ?>
                                                    <li><a href="http://www.cipres.co.uk/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'es': ?>
                                                    <li><a href="http://cipres-filtr-brno.czechtrade.es/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'pl': ?>
                                                    <li><a href="http://www.cipres.pl/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'it': ?>
                                                    <li><a href="http://www.cipres.it/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'sk': ?>
                                                    <li><a href="http://www.cipres.sk/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'ua': ?>
                                                    <li><a href="http://www.cipres.com.ua/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'fr': ?>
                                                    <li><a href="http://cipres-filtr-brno.czech-trade.fr/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                default:
                                                    break;
                                            }
                                        } else { ?>
                                            <li><a href="<?= $jazyk['url'] ?>"><img
                                                            src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                            class="value">1</span></a></li>
                                        <?php }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="social_icon_holder">
                        <a href="http://www.facebook.com/CipresFiltrBrno" target="_blank">
                            <i class="fa fa-facebook-official" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </nav>
            <div class="rp_breadcrumb">
                <?php Extras\custom_breadcrumbs(); ?>
            </div>
        </div>
        <div class="rp_czech_flag_holder clearfix">
            <div class="rp_czech_flag_text"><?= __('Made in<br>czech republic', 'cipres') ?></div>
        </div>
        <div class="napiste_nam_holder">
            <a href="#test-popup" class="open-popup-link"><?= __('Write us', 'cipres') ?></a>
        </div>
    </div>
    <div class="container header_mobile">

        <div class="row no-gutters header_mobile_logo_holder">
            <div class="col-9 header_mobile_logo_left">
                <?php
                if (function_exists('the_custom_logo')) {
                    the_custom_logo();
                }
                ?>
            </div>
            <div class="col-3 header_mobile_logo_right">
                <div class="header_mobile_select">
                    <div class="language_holder">
                        <div class="drop-down-select">
                            <div class="selected">
                                <?php
                                foreach ($jazyky as $jazyk) {
                                    if ($jazyk['active'] == 1): ?>
                                        <a href="#"><span><img
                                                        src="<?= $jazyk['country_flag_url'] ?>" alt=""></span></a>
                                    <?php endif;
                                } ?>
                            </div>
                            <div class="options">
                                <ul>
                                    <?php
                                    foreach ($jazyky as $jazyk) {
                                        if (isset($jazyk['missing']) && $jazyk['missing'] == 1) {
                                            switch ($jazyk['code']) {
                                                case 'bg': ?>
                                                    <li><a href="//seznam.cz"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'nl': ?>
                                                    <li><a href="http://www.cipres.de.com/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'de': ?>
                                                    <li><a href="http://www.cipres.de.com/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'ru': ?>
                                                    <li><a href="http://www.cipres.ru/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'ch': ?>
                                                    <li><a href="http://www.cipres.de.com/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'en': ?>
                                                    <li><a href="http://www.cipres.co.uk/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'es': ?>
                                                    <li><a href="http://cipres-filtr-brno.czechtrade.es/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'pl': ?>
                                                    <li><a href="http://www.cipres.pl/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'it': ?>
                                                    <li><a href="http://www.cipres.it/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'sk': ?>
                                                    <li><a href="http://www.cipres.sk/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'ua': ?>
                                                    <li><a href="http://www.cipres.com.ua/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                case 'fr': ?>
                                                    <li><a href="http://cipres-filtr-brno.czech-trade.fr/"><img
                                                                    src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                                    class="value">1</span></a></li>
                                                    <?php break;
                                                default:
                                                    break;
                                            }
                                        } else { ?>
                                            <li><a href="<?= $jazyk['url'] ?>"><img
                                                            src="<?= $jazyk['country_flag_url'] ?>" alt=""><span
                                                            class="value">1</span></a></li>
                                        <?php }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="header_mobile_menu_btn">
                        <div class="mobile_menu_btn">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutters header_mobile_slogan">
            <div><?= get_bloginfo('description') ?></div>
        </div>
        <div class="row no-gutters">
            <div class="col-12 header_mini_menu_holder">
                <ul>
                    <?php
                    $menuLocations = get_nav_menu_locations();
                    $menuID = $menuLocations['mobile_navigation'];
                    $mobileNav = wp_get_nav_menu_items($menuID);

                    global $wp;
                    $current_url = home_url($wp->request);
                    foreach ($mobileNav as $item) { ?>
                        <li class="nav-item <?= $current_url . '/' == $item->url ? 'active' : '' ?>">
                            <a class="nav-link" href="<?= $item->url ?>"><?= $item->title ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="header_main_mobile_menu">

            <div class="header_main_close_btn">
                <a href="#">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>

            <ul>
                <?php
                $menuLocations = get_nav_menu_locations();
                $menuID = $menuLocations['primary_navigation'];
                $primaryNav = wp_get_nav_menu_items($menuID);

                global $wp;
                $current_url = home_url($wp->request);
                foreach ($primaryNav as $item) { ?>
                    <li class="nav-item <?= $current_url . '/' == $item->url ? 'active' : '' ?>">
                        <a class="nav-link" href="<?= $item->url ?>"><?= $item->title ?></a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>

        <div class="rp_czech_flag_holder clearfix">
            <div class="rp_czech_flag_text"><?= __('Made in<br>czech republic', 'cipres') ?></div>
        </div>
        <div class="napiste_nam_holder">
            <a href="#test-popup" class="open-popup-link"><?= __('Write us', 'cipres') ?></a>
        </div>

    </div>

    <div id="test-popup" class="rp_napiste_popup_holder mfp-hide">
        <?= do_shortcode('[contact-form-7 id="246" title="Napište nám"]') ?>
    </div>
</header>
