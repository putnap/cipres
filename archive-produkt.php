<div class="rp_produkty_editor_text">
    <?= get_field('text_podstranky_produkty',get_option( 'page_on_front' )) ?>
</div>

<div class="rp_produkty_holder">
    <div class="row rp_produkty_row">
    <?php
    $index = 0;
    while (have_posts()) : the_post(); ?>

            <div class="col-md-<?= $index<2?'6':'4' ?> col-12">
                <div class="rp_produkty_item">
                    <h4><a href="<?= get_permalink() ?>"><?= get_the_title() ?></a></h4>
                    <div class="rp_produkt_info_text row no-gutters">
                        <div class="col-lg-7">
                            <p><?= get_field('kratky_popis')?></p>
                            <a class="rp_produkt_more_btn" href="<?= get_permalink() ?>"><?= __('read more','RP')?></a>
                        </div>
                        <div class="col-lg-5 text-center">
                            <a href="<?= get_permalink() ?>">
                                <?= get_the_post_thumbnail(get_the_ID(),'produkt') ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
    <?php
    $index++;
    endwhile; ?>
	</div>
</div>

